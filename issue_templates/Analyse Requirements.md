## Summary

### What?

(Describe what needs to be investigated and/or analyzed)

### Why?

(What makes this issue worthy of analysis?)

### Interests?

(What are some interesting topics or suggestions that could be expanded in this analysis?)

## Useful ressources

(Add all exemples, guides, tools, specifications or documentations you can about you request)

/label ~"type::analysis" ~"Require analysis" ~"RFC" ~"sprint::unscheduled" ~"weight::dry"
/cc @flecavalier
/assign @vlaurent
