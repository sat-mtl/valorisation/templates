# Issue & merge request templates

This repository contain all templates used for every projects at the valorisation of the research department. These templates are used to configure the types of issues and merge requests in Gitlab according to this [documentation](https://docs.gitlab.com/ee/user/project/description_templates.html).

As [the group-level description templates](https://docs.gitlab.com/ee/user/project/description_templates.html#set-group-level-description-templates) is only a _premium_ feature we use the [`git subtree`](https://www.atlassian.com/git/tutorials/git-subtree) command in order to synchronize these templates across all projects maintained by the team.

## How to install the templates

All commands should be executed at the root of the maintained project: it uses [this repository](https://gitlab.com/sat-mtl/telepresence/templates) as a git remote and it will clone it into the `.gitlab` folder.

```bash
git remote add -f templates https://gitlab.com/sat-mtl/telepresence/templates.git
git subtree add --prefix .gitlab templates main --squash
```

## How to update the templates

If this repository has been updated, you should update the repository for each project. Ensure that the subtree is installed in your local copy of the project you want to synchronize with these templates.

```bash
git subtree pull --prefix .gitlab templates main --squash
```
